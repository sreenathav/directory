package com.employee.directoy.telephone.repository;

import com.employee.directoy.telephone.model.Phone;
import com.employee.directoy.telephone.model.PhoneModel;
import com.employee.directoy.telephone.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(SpringExtension.class)
public class UserCustomRepositoryTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserCustomRepositoryImpl userCustomRepository;

    @Test
    public void testAddPhones() {

        String sampleUserId = "123";
        User userRecord = new User();
        userRecord.setUserId("123");
        userRecord.setUsername("username1");
        userRecord.setPhones(new ArrayList() {
            {
                add(new Phone("2344"));
            }});

        Phone phone1 = new Phone("678");
        phone1.setPhoneModel(PhoneModel.ANDROID);

        User userUpdate = new User(sampleUserId);
        userUpdate.setPhones(Arrays.asList(phone1));

        Mockito.when(userRepository.findById(sampleUserId)).thenReturn(java.util.Optional.of(userRecord));
        User user = userCustomRepository.addPhone(userUpdate);

        Mockito.verify(userRepository, Mockito.times(1)).save(userRecord);
    }

}
