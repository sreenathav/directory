package com.employee.directoy.telephone.controller;

import com.employee.directoy.telephone.model.User;
import com.employee.directoy.telephone.repository.UserCustomRepositoryImpl;
import com.employee.directoy.telephone.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TelephoneController.class)
public class TelephoneControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;


    @MockBean
    UserCustomRepositoryImpl userRepository;

    @Test
    public void addUserDetailsTest() throws Exception {

        User user1 = new User("123");
        Mockito.when(userRepository.save(user1)).thenReturn(user1);

        mockMvc.perform( MockMvcRequestBuilders.post("/telephone/user")
        .content(mapper.writeValueAsString(user1)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    //TODO: More tests to add.
}
