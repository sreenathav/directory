package com.employee.directoy.telephone.controller;

import com.employee.directoy.telephone.model.User;
import com.employee.directoy.telephone.repository.UserCustomRepositoryImpl;
import com.employee.directoy.telephone.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/telephone")
@Slf4j
public class TelephoneController {

    @Autowired
    UserCustomRepositoryImpl userCustomRepository;

    @PostMapping("/user")
    public User addUserDetails(@RequestBody User user) {
        log.info("Saving user with id {}", user.getUserId());
        return userCustomRepository.save(user);
    }

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userCustomRepository.findAll();
    }

    @GetMapping("/user/{userid}")
    public Optional<User> getUserById(@PathVariable("userid") String userId) {
        return userCustomRepository.findById(userId);
    }

    @DeleteMapping("/user/{userid}")
    public Optional<User> deleteUserDetails(@PathVariable("userid") String userId) {
        Optional<User> user = userCustomRepository.findById(userId);
        userCustomRepository.delete(userId);
        return user;
    }

    @PutMapping("/user/phone")
    public User addUserPhone(@RequestBody User user) {
        return userCustomRepository.addPhone(user);
    }

    @DeleteMapping("/user/{userId}/phone/{phoneId}")
    public User deleteUserPhone(@PathVariable("userId") String userId, @PathVariable("phoneId") String phoneId) {
        return userCustomRepository.deletePhone(userId, phoneId);
    }



}
