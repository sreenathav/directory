package com.employee.directoy.telephone.repository;

import com.employee.directoy.telephone.model.User;

import java.util.List;
import java.util.Optional;

public interface UserCustomRepository {

    public User addPhone(User user);

    public User deletePhone(String userId, String phoneId);

    User save(User user);

    List<User> findAll();

    Optional<User> findById(String userId);

    void delete(String userId);
}
