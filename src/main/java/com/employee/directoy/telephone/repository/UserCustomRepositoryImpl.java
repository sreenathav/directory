package com.employee.directoy.telephone.repository;

import com.employee.directoy.telephone.model.Phone;
import com.employee.directoy.telephone.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository{

    @Autowired
    UserRepository userRepository;
    
    @Override
    public User addPhone(User user) {

        Optional<User> userRecord = userRepository.findById(user.getUserId());
        if(userRecord.isPresent()) {
            userRecord.get().getPhones().addAll(user.getPhones());
            return userRepository.save(userRecord.get());
        }

        return null;
    }

    @Override
    public User deletePhone(String userId, String phoneId) {
        Optional<User> userRecord = userRepository.findById(userId);
        if(userRecord.isPresent()) {
            userRecord.get().getPhones().remove(new Phone(phoneId));
            return userRepository.save(userRecord.get());
        }
        return null;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findById(String userId) {
        return userRepository.findById(userId);
    }

    @Override
    public void delete(String userId) {
        userRepository.delete(new User(userId));
    }
}
