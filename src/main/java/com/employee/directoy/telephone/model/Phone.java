package com.employee.directoy.telephone.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Entity
@Table(name = "phones")
public class Phone implements Serializable {

    public Phone(String id) {
        this.phoneId = id;
    }

    public Phone(){};

    private static final long serialVersionUID = 1L;

    @Column(name = "phone_id")
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String phoneId;

    @Column(name = "phone_name")
    private String phoneName;

    @Column(name = "phone_model")
    @Enumerated(EnumType.STRING)
    private PhoneModel phoneModel;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "pref_phone_flag")
    private boolean prefPhoneFlag;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return phoneId.equals(phone.phoneId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneId);
    }
}
