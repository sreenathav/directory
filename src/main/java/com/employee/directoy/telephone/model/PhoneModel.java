package com.employee.directoy.telephone.model;

import lombok.Data;

public enum PhoneModel {
    IPHONE("iPhone"), ANDROID("Android"), DESK_PHONE("Desk Phone"), SOFT_PHONE("Soft Phone");

    private final String phoneModel;

    PhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }
}
